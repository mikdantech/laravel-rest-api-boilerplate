<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Customer;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::factory()
            ->count(5)
            ->hasInvoices(10)
            ->create();

        Customer::factory()
            ->count(5)
            ->hasInvoices(5)
            ->create();

        Customer::factory()
            ->count(3)
            ->hasInvoices(3)
            ->create();

        Customer::factory()
            ->count(1)
            ->create();
    }
}
