<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'id' => '97aa6b84-73eb-4f08-bf40-b2ed0fd3f1ff',
            'name' => 'Admin 1',
            'email' => 'admin1@example.com',
            'password' => Hash::make('password'),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'secret_value' => 'Love the code !!'
        ]);
        $user1->createToken('Admin 1 Token')->plainTextToken;

        $user2 = User::create([
            'id' => '97aa7760-22b4-412c-9ba7-c11affe0e9ac',
            'name' => 'Admin 2',
            'email' => 'admin2@example.com',
            'password' => Hash::make('password'),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'secret_value' => 'Fuck the code !!'
        ]);
        $user2->createToken('Admin 2 Token')->plainTextToken;
        $user2->roles()->attach(Role::where('slug', 'admin')->first());

        User::factory()
            ->count(5)
            ->create();
    }
}
