<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'user_management_access',
            ],
            [
                'name' => 'permission_create',
            ],
            [
                'name' => 'permission_edit',
            ],
            [
                'name' => 'permission_show',
            ],
            [
                'name' => 'permission_delete',
            ],
            [
                'name' => 'permission_access',
            ],
            [
                'name' => 'role_create',
            ],
            [
                'name' => 'role_edit',
            ],
            [
                'name' => 'role_show',
            ],
            [
                'name' => 'role_delete',
            ],
            [
                'name' => 'role_access',
            ],
            [
                'name' => 'user_create',
            ],
            [
                'name' => 'user_edit',
            ],
            [
                'name' => 'user_show',
            ],
            [
                'name' => 'user_delete',
            ],
            [
                'name' => 'user_access',
            ],
            [
                'name' => 'faq_management_access',
            ],
            [
                'name' => 'faq_category_create',
            ],
            [
                'name' => 'faq_category_edit',
            ],
            [
                'name' => 'faq_category_show',
            ],
            [
                'name' => 'faq_category_delete',
            ],
            [
                'name' => 'faq_category_access',
            ],
            [
                'name' => 'faq_question_create',
            ],
            [
                'name' => 'faq_question_edit',
            ],
            [
                'name' => 'faq_question_show',
            ],
            [
                'name' => 'faq_question_delete',
            ],
            [
                'name' => 'faq_question_access',
            ],
        ];

        collect($permissions)->each(function ($permission) {
            Permission::create($permission);
        });
    }
}
