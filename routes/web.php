<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return ['App' => app()->version()];
});

Route::get('/node/{host?}', function ($host = 'localhost:6000') {
    $response = Http::get('http://' . $host);
    return $response;
});

require __DIR__ . '/auth.php';
