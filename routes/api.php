<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum', 'verified'])->get('/user', function (Request $request) {
    return $request->user();
});

// api/v1
Route::group(['prefix' => 'v1', 'as' => 'api', 'middleware' => ['auth:sanctum', 'verified'], 'namespace' => 'App\Http\Controllers\Api\V1'], function () {

    Route::get('/profile', ProfileController::class);
    Route::put('/profile', [ProfileController::class, 'update']);

    Route::apiResource('/articles', ArticleController::class);
    Route::get('/article/author/{user}', [AuthorController::class, 'show'])->name('authors');

    Route::apiResource('/customers', CustomerController::class);
    Route::apiResource('/invoices', InvoiceController::class);
    Route::post('/invoices/bulk', ['uses' => 'InvoiceController@bulkStore']);

    // Faq Category
    Route::apiResource('/faq-categories', FaqCategoryApiController::class);
    // Faq Question
    Route::apiResource('/faq-questions', FaqQuestionApiController::class);

    // Product Category
    Route::post('/product-categories/media', 'ProductCategoryApiController@storeMedia')->name('product-categories.storeMedia');
    Route::apiResource('/product-categories', ProductCategoryApiController::class);

    // Product Tag
    Route::apiResource('/product-tags', ProductTagApiController::class);

    // Product
    Route::post('/products/media', 'ProductApiController@storeMedia')->name('products.storeMedia');
    Route::apiResource('/products', ProductApiController::class);
});

Route::group(['as' => 'api', 'middleware' => ['auth:sanctum', 'verified'], 'namespace' => 'App\Http\Controllers\Auth'], function () {
    // Abilities
    Route::apiResource('/abilities', AbilitiesController::class, ['only' => ['index']]);

    // Locales
    Route::get('/locales/languages', 'LocalesController@languages')->name('locales.languages');
    Route::get('/locales/messages', 'LocalesController@messages')->name('locales.messages');

    // Permissions
    Route::apiResource('/permissions', PermissionsApiController::class);

    // Roles
    Route::apiResource('/roles', RolesApiController::class);

    // Users
    Route::apiResource('/users', UsersApiController::class);
});
