<?php

namespace App\Models;

use \DateTimeInterface;
use App\Helpers\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class FaqCategory extends Model
{
    use HasAdvancedFilter;
    use SoftDeletes;
    use HasFactory;
    use HasUuids;

    public $table = 'faq_categories';

    protected $fillable = [
        'category',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $orderable = [
        'category',
        'created_at',
        'updated_at',
    ];

    protected $filterable = [
        'category',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
