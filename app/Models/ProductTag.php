<?php

namespace App\Models;

use \DateTimeInterface;
use App\Helpers\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class ProductTag extends Model
{
    use HasAdvancedFilter;
    use SoftDeletes;
    use HasFactory;
    use HasUuids;

    public $table = 'product_tags';

    protected $orderable = [
        'name',
        'created_at',
        'updated_at',
    ];

    protected $filterable = [
        'name',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
