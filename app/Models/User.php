<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use App\Traits\ModelHelpers;
use Illuminate\Database\Eloquent\Relations\HasMany;

use \DateTimeInterface;
use App\Helpers\HasAdvancedFilter;
use Carbon\Carbon;
// use Hash;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, HasUuids, ModelHelpers, HasAdvancedFilter, SoftDeletes;


    public $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'deleted_at',
        'secret_value',
    ];


    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    // protected $visible = ['name', 'email'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'email_verified_at',
        'deleted_at',
        // Custom
        'secret_value',
    ];

    protected $orderable = [
        'name',
        'email',
        'email_verified_at',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'email_verified_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $filterable = [
        'name',
        'email',
        'email_verified_at',
        'roles.name',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'secret_value' => 'encrypted',
    ];

    public function getIsAdminAttribute()
    {
        return $this->roles()->where('slug', 'admin')->exists();
    }

    public function getIsSuperAdminAttribute()
    {
        return $this->roles()->where('slug', 'super-admin')->exists();
    }

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = Hash::needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function articles(): HasMany
    {
        return $this->hasMany(Article::class, 'author_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
