<?php

namespace App\Models;

use \DateTimeInterface;
use App\Helpers\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class FaqQuestion extends Model
{
    use HasAdvancedFilter;
    use SoftDeletes;
    use HasFactory;
    use HasUuids;

    public $table = 'faq_questions';

    protected $fillable = [
        'category_id',
        'question',
        'answer',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $orderable = [
        'category.category',
        'question',
        'answer',
        'created_at',
        'updated_at',
    ];

    protected $filterable = [
        'category.category',
        'question',
        'answer',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function category()
    {
        return $this->belongsTo(FaqCategory::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
