<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\StorePermissionRequest;
use App\Http\Requests\Auth\UpdatePermissionRequest;
use App\Http\Resources\Auth\PermissionResource;
use App\Models\Permission;
use Illuminate\Support\Facades\Gate;
// use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;
use App\Exceptions\ForbiddenException;


class PermissionsApiController extends Controller
{

    public function index()
    {
        if (Gate::denies('permission_access'))
            throw new ForbiddenException();

        return new PermissionResource(Permission::advancedFilter());
    }

    public function store(StorePermissionRequest $request)
    {
        $permission = Permission::create($request->validated());

        return (new PermissionResource($permission))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Permission $permission)
    {
        if (Gate::denies('permission_show'))
            throw new ForbiddenException();

        return new PermissionResource($permission);
    }

    public function update(UpdatePermissionRequest $request, Permission $permission)
    {
        $permission->update($request->validated());

        return (new PermissionResource($permission))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Permission $permission)
    {
        if (Gate::denies('permission_delete'))
            throw new ForbiddenException();

        $permission->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
