<?php

namespace App\Http\Requests\V1;

use App\Models\FaqQuestion;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateFaqQuestionRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('faq_question_edit');
    }

    public function rules()
    {
        return [
            'category_id' => [
                'integer',
                'exists:faq_categories,id',
                'required',
            ],
            'question' => [
                'string',
                'required',
            ],
            'answer' => [
                'string',
                'required',
            ],
        ];
    }
}