<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

use Laravel\Sanctum\Exceptions\MissingAbilityException;

use App\Exceptions\ForbiddenException;

use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $exception) {
            //
        });

        $this->renderable(function (Throwable $exception, $request) {
            if (!empty($exception)) {
                // set default error message
                $response = [];
                $status = 500;
                // If debug mode is enabled
                if (config('app.debug')) {
                    $response['exception'] = get_class($exception); // Reflection might be better here
                    $response['audit'] = $exception->getMessage();
                    // $response['trace'] = $exception->getTrace();
                }
                if ($exception instanceof AuthenticationException) {
                    $status = 401;
                    $response['message'] = 'Can not finish authentication!';
                } else if (
                    $exception instanceof AccessDeniedHttpException or
                    $exception instanceof MethodNotAllowedHttpException or
                    $exception instanceof ThrottleRequestsException or
                    $exception instanceof TokenMismatchException or
                    $exception instanceof MissingAbilityException or
                    $exception instanceof ModelNotFoundException or
                    $exception instanceof ForbiddenException
                ) {
                    $status = $exception->getStatusCode();
                    $response['message'] = $exception->getMessage();
                } else if ($exception instanceof ValidationException) {
                    $status = 400;
                    $response['message'] = $exception->getMessage();
                } else if ($exception instanceof NotFoundHttpException) {
                    $status = $exception->getStatusCode();
                    $response['message'] = 'Not Found';
                } else if ($exception instanceof QueryException) {
                    $status = 500;
                    $response['message'] = 'Unexpected Error, Please contact the administrator';
                }
                // Unhandled Request
                else if ($this->isHttpException($exception)) {
                    $status = 500;
                    $response['message'] = 'Unhandled Request error!, Please contact the administrator';
                } else {
                    // for all others check do we have method getStatusCode and try to get it
                    // otherwise, set the status to 400
                    $status = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : 500;
                    $response['message'] = $exception->getMessage();
                }

                // Universal Error Message
                return response()->json([
                    'error' => $response
                ], $status);
            }
        });
    }
}
