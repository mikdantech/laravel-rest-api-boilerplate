<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;

class ForbiddenException extends ValidationException
{
    public $status = 403;

    public function __construct($message = null)
    {
        $validator = Validator::make([], []);
        $validator->errors()->add('fieldName', $message ?? "You don't have permission to access");
        parent::__construct($validator);
    }

    public function getStatusCode()
    {
        return $this->status;
    }
}
