
git clone https://gitlab.com/mikdantech/laravel-rest-api-boilerplate.git Laravel-REST-API-Boilerplate


rm composer.lock
rm -rf vendor

`composer install # Dev or Test`

`composer install --optimize-autoloader --no-dev # Production`

chmod u+x artisan

php artisan key:generate

rm storage/logs/laravel.log

## Maintenance mode
php artisan down
php artisan up

php artisan migrate:fresh
php artisan migrate:refresh
php artisan migrate
php artisan migrate:status
php artisan db:seed

php artisan migrate:fresh --seed

php artisan route:clear
php artisan route:list

php artisan optimize:clear
php artisan cache:clear
php artisan view:clear
php artisan clear-compiled
php artisan route:clear

php artisan serve --port=8000

php artisan 

/api/v1/customers
/api/v1/invoices


/sanctum/csrf-cookie
/api/user
/verify-email
/register
/login
/forgot-password
/reset-password
/email/verification-notification
/logout


composer -V
mkdir bin
wget https://getcomposer.org/composer-stable.phar -O ~/bin/composer.phar
cd bin
mv composer.phar composer
cd ~
chmod u+x bin/composer
vim .bash_profile
composer -V


php artisan make:controller API/V1/ProfileController -m User
php artisan make:resource V1/UserResource

php artisan make:controller API/V1/ArticleController -m Article --api

php artisan make:controller API/V1/AuthorController -m User

php artisan make:resource V1/ArticleResource
php artisan make:resource V1/ArticleCollection -c

php artisan make:resource V1/AuthorResource


php artisan make:notification ResetPasswordNotification

php artisan make:model Article -m
php artisan make:factory ArticleFactory
php artisan make:seed ArticleSeeder

php artisan make:seed UserSeeder
php artisan make:seed PermissionSeeder
php artisan make:seed RoleUserSeeder
php artisan make:seed PermissionRoleSeeder


php artisan make:exception ForbiddenException

php artisan make:migration CreateMedia
php artisan make:migration CreatePermissions
php artisan make:migration CreateRolePermissions
php artisan make:migration CreateFaqCategoriesTable
php artisan make:migration CreateFaqQuestionsTable

php artisan make:migration CreateProductCategoriesTable
php artisan make:migration CreateProductTagsTable
php artisan make:migration CreateProductsTable


# Login - Response Headers:
`Set-Cookie: accessToken=ramdom-token; Path=/; Expires=Sun, 06 Nov 2022 14:04:17 GMT; HttpOnly; Secure; SameSite=None`

# Resource - Request Headers:
`Cookie: accessToken=ramdom-token`
